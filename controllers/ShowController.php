<?php
include_once ROOT . '/models/Show.php';

class ShowController
{
    /**
     * create new click item
     * @return bool
     * @throws exception
     */
    public function actionNew()
    {
        if (isset($_POST['resource_id'])) {
            $show = array();

            $show['resource_id'] = $_POST['resource_id'];
            $show['ads_id'] = $_POST['ads_id'];
            $show['user_ip'] = $_POST['user_ip'];
            $show['country_code'] = $_POST['country_code'];

            Show::insert($show);
        }

        return true;
    }


    /**
     * display show list
     * @return bool
     */
    public function actionIndex()
    {
        $showList = array();
        $showList = Show::getShowList();

        require_once(ROOT . '/views/show/index.php');

        return true;
    }

}