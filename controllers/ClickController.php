<?php

include_once ROOT . '/models/Click.php';
include_once ROOT . '/models/Ads.php';

class ClickController
{
    /**
     * create new click item
     * @return bool
     * @throws exception
     */
    public function actionNew()
    {
        if (isset($_POST['resource_id'])) {
            $click = array();

            $click['resource_id'] = $_POST['resource_id'];
            $click['ads_id'] = $_POST['ads_id'];
            $click['user_ip'] = $_POST['user_ip'];
            $click['country_code'] = $_POST['country_code'];

            Click::insert($click);
        }

        return true;
    }

    /**
     * show click list
     * @return bool
     */
    public function actionIndex()
    {
        $clickList = array();
        $clickList = Click::getClickList();

        require_once(ROOT . '/views/click/index.php');

        return true;
    }
}