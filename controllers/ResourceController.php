<?php

include_once ROOT . '/models/Resource.php';
include_once ROOT . '/models/Topic.php';

class ResourceController
{
    const ITEMS_PER_PAGE = 15;

    /**
     * redirect to index
     */
    private function redirectToIndex()
    {
        header('Location: /resource/index/1');
    }

    /**
     * add new item
     * @return bool
     */
    public function actionNew()
    {
        if (!isset($_POST['submit'])) {
            $topics = array();
            $topics = Topic::getTopicList();
        } else {
            $resource = array();

            $resource['url'] = htmlspecialchars($_POST['url']);
            $resource['topic_id'] = htmlspecialchars($_POST['topic']);
            $resource['contact'] = htmlspecialchars($_POST['contact']);

            Resource::insert($resource);

            $this->redirectToIndex();
            exit;
        }

        require_once(ROOT . '/views/resource/new.php');

        return true;
    }

    /**
     * edit item
     * @param int $id
     * @return bool
     */
    public function actionEdit($id)
    {
        if ($id) {
            if (isset($_POST['submit'])) {
                $resource = array();

                $resource['id'] = $id;
                $resource['url'] = htmlspecialchars($_POST['url']);
                $resource['topic_id'] = htmlspecialchars($_POST['topic']);
                $resource['contact'] = htmlspecialchars($_POST['contact']);

                Resource::update($resource);

                $this->redirectToIndex();
                exit;
            }

            $topics = array();
            $topics = Topic::getTopicList();

            $resourceItem = Resource::getResourceItemById($id);

            require_once(ROOT . '/views/resource/edit.php');
        }

        return true;
    }

    /**
     * remove item
     * @return bool
     */
    public function actionDelete($id)
    {
        if ($id) {
            Resource::delete($id);

            $this->redirectToIndex();
            exit;
        }

        return true;
    }

    /**
     * show resource list
     * @return bool
     */
    public function actionIndex($page)
    {
        $offset = ($page - 1) * self::ITEMS_PER_PAGE;

        $resources = array();
        $resources = Resource::getResourceList();

        $count = count($resources);
        $pagesCount = ceil($count / self::ITEMS_PER_PAGE);

        $resourceList = array();

        $maxOffset = $offset + self::ITEMS_PER_PAGE;
        $limit =  $maxOffset <= $count ? $maxOffset : $count;
        for ($i = $offset; $i < $limit; $i++) {
            $resourceList[] = $resources[$i];
        }

        require_once(ROOT . '/views/resource/index.php');

        return true;
    }
}