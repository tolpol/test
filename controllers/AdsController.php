<?php

include_once ROOT . '/models/Resource.php';
include_once ROOT . '/models/Ads.php';

class AdsController
{
    /**
     * redirect to index
     */
    private function redirectToIndex()
    {
        header('Location: /ads/index');
    }

    /**
     * create new ads item
     * @return bool
     */
    public function actionNew()
    {
        if (!isset($_POST['submit'])) {
            $resources = array();
            $resources = Resource::getResourceIds();
        } else {
            $ads = array();

            $ads['title'] = htmlspecialchars($_POST['title']);
            $ads['resource_id'] = intval(htmlspecialchars($_POST['resource_id']));
            $ads['description'] = htmlspecialchars($_POST['description']);

            Ads::insert($ads);

            $this->redirectToIndex();
            exit;
        }

        require_once(ROOT . '/views/ads/new.php');

        return true;
    }

    /**
     * edit ads item by id
     * @return bool
     */
    public function actionEdit($id)
    {
        if ($id) {
            if (isset($_POST['submit'])) {
                $ads = array();

                $ads['id'] = $id;
                $ads['title'] = htmlspecialchars($_POST['title']);
                $ads['resource_id'] = intval(htmlspecialchars($_POST['resource_id']));
                $ads['description'] = htmlspecialchars($_POST['description']);

                Ads::update($ads);

                $this->redirectToIndex();
                exit;
            }

            $adsItem = Ads::getAdsItemById($id);

            $resources = array();
            $resources = Resource::getResourceIds();

            require_once(ROOT . '/views/ads/edit.php');
        }

        return true;
    }

    /**
     * remove ads item by id
     */
    public function actionDelete($id)
    {
        if ($id) {
            Ads::delete($id);

            $this->redirectToIndex();
            exit;
        }

        return true;
    }

    /**
     * show ads list
     * @return bool
     */
    public function actionIndex()
    {
        $adsList = array();
        $adsList = Ads::getAdsList();

        require_once(ROOT . '/views/ads/index.php');

        return true;
    }
}