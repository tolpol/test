<?php require_once(ROOT . '/views/header.php'); ?>

    <table class="table table-striped table-responsive" id="common-statistic">
        <thead>
        <tr class="row">
            <th class="col-lg-2">ID интернет ресурса</th>
            <th class="col-lg-2">ID рекламного блока</th>
            <th class="col-lg-2">Дата показа</th>
            <th class="col-lg-2">IP пользователя</th>
            <th class="col-lg-2">ISO код страны</th>
            <th class="col-lg-2">Количество</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($showList as $showItem): ?>
            <tr class="row">
                <td class="col-lg-2"><?php echo $showItem['resource_id']; ?></td>
                <td class="col-lg-2"><?php echo $showItem['ads_id']; ?></td>
                <td class="col-lg-2"><?php echo $showItem['date_show']; ?></td>
                <td class="col-lg-2"><?php echo $showItem['user_ip']; ?></td>
                <td class="col-lg-2"><?php echo $showItem['country_code']; ?></td>
                <td class="col-lg-2"><?php echo $showItem['quantity']; ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

<?php require_once(ROOT . '/views/footer.php'); ?>