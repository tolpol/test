<?php require_once(ROOT . '/views/header.php'); ?>

    <table class="table table-condensed">
        <caption>Фильтры</caption>
        <thead>
        </thead>
        <tbody>
            <tr class="row">
                <td class="col-lg-2">
                    <label for="min_date">мин. дата</label>
                    <input type="text" id="min_date" value="" class="form-control" onkeyup="filter()">
                    <label for="max_date">макс. дата</label>
                    <input type="text" id="max_date" value="" class="form-control" onkeyup="filter()">
                </td>
                <td class="col-lg-2">
                    <label for="ads_id">ID рекламного блока</label>
                    <input type="text" id="ads_id" value="" class="form-control" onkeyup="filter()">
                </td>
                <td class="col-lg-2">
                    <label for="resource_id">ID интернет ресурса</label>
                    <input type="text" id="resource_id" value="" class="form-control" onkeyup="filter()">
                </td>
                <td class="col-lg-2">
                    <label for="country_code">ISO код страны</label>
                    <input type="text" id="country_code" value="" class="form-control" onkeyup="filter()">
                </td>
            </tr>
        </tbody>
    </table>

    <table class="table table-striped table-responsive" id="common-statistic">
        <thead>
        <tr class="row">
            <th class="col-lg-1">Дата показа</th>
            <th class="col-lg-3">ID рекламного блока</th>
            <th class="col-lg-3">ID интернет ресурса</th>
            <th class="col-lg-3">ISO код страны</th>
            <th class="col-lg-3">Количество показов</th>
            <th class="col-lg-3">Количество кликов</th>
        </tr>
        </thead>
        <tbody id="t-body"></tbody>
    </table>

<?php if (isset($summaryList)): ?>
    <script type="text/javascript">
        var table = <?php echo json_encode($summaryList); ?>;
    </script>
<?php endif; ?>

<?php require_once(ROOT . '/views/footer.php'); ?>