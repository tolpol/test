<?php require_once(ROOT . '/views/header.php'); ?>

    <div class="col-lg-3"></div>
    <div class="col-lg-6">
    <form action="" method="post" name="resource-form" class="form">
        <div class="form-group">
            <label for="url">URL</label>
            <input type="url" name="url" class="form-control" id="url" required>
        </div>
        <div class="form-group">
            <label for="topic">Тематика</label>
            <select id="topic" name="topic" class="form-control">
            <?php foreach ($topics as $topic): ?>
                <option value="<?php echo $topic['id']; ?>"><?php echo $topic['title']; ?></option>
            <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="contact">Контактная информация</label>
            <input type="text" name="contact" class="form-control" id="contact" required>
        </div>
        <input type="submit" name="submit" class="btn btn-primary" value="save">
    </form>
    </div>
    <div class="col-lg-3"></div>

<?php require_once(ROOT . '/views/footer.php'); ?>