<?php require_once(ROOT . '/views/header.php'); ?>

    <table class="table table-striped table-responsive">
        <thead>
        <tr class="row">
            <th class="col-lg-1">id</th>
            <th class="col-lg-2">Название</th>
            <th class="col-lg-1">ID нтернет ресурса</th>
            <th class="col-lg-5">Описание</th>
            <th class="col-lg-3">
                <a class="btn btn-success pull-right" href="/ads/new">new</a>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($adsList as $adsItem): ?>
            <tr class="row">
                <td class="col-lg-1"><?php echo $adsItem['id']; ?></td>
                <td class="col-lg-2"><?php echo $adsItem['title']; ?></td>
                <td class="col-lg-1"><?php echo $adsItem['resource_id']; ?></td>
                <td class="col-lg-5"><?php echo $adsItem['description']; ?></td>
                <td class="col-lg-3">
                    <button class="btn btn-warning" onclick="showAdsCode(this)">code</button>
                    <a class="btn btn-danger pull-right delete"
                       href="/ads/delete/<?php echo $adsItem['id']; ?>" onclick="deleteItem()">delete</a>
                    <a class="btn btn-primary pull-right" href="/ads/edit/<?php echo $adsItem['id']; ?>">edit</a
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

<?php require_once(ROOT . '/views/footer.php'); ?>




