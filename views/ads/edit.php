<?php require_once(ROOT . '/views/header.php'); ?>

    <div class="col-lg-3"></div>
    <div class="col-lg-6">
        <form action="" method="post" name="resource-form" class="form">
            <div class="form-group">
                <label for="title">Название блока</label>
                <input type="text" name="title" class="form-control" id="title" required
                       value="<?php echo $adsItem['title'];?>">
            </div>
            <div class="form-group">
                <label for="resource-id">ID Интернет ресурса</label>
                <select name="resource_id" id="resource-id" class="form-control">
                    <?php foreach ($resources as $resource): ?>

                        <?php $selected = $resource['id'] == $adsItem['resource_id'] ? 'selected' : ''; ?>
                        <option <?php echo $selected; ?> value="<?php echo $resource['id']; ?>">
                            <?php echo $resource['id']; ?>
                        </option>

                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="contact">Контактная информация</label>
                <input type="text" name="description" class="form-control" id="contact" required
                       value="<?php echo $adsItem['description'];?>">
            </div>
            <input type="submit" class="btn btn-primary" name="submit" value="save">
        </form>
    </div>
    <div class="col-lg-3"></div>

<?php require_once(ROOT . '/views/footer.php'); ?>