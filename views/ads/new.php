<?php require_once(ROOT . '/views/header.php'); ?>

    <div class="col-lg-3"></div>
    <div class="col-lg-6">
    <form action="" method="post" name="ads-form" class="form">
        <div class="form-group">
            <label for="title">Название блока</label>
            <input type="text" name="title" class="form-control" id="title" required>
        </div>
        <div class="form-group">
            <label for="resource-id">ID интернет ресурса</label>
            <select name="resource_id" id="resource-id" class="form-control">
                <?php foreach ($resources as $resource): ?>
                    <option value="<?php echo $resource['id']; ?>"><?php echo $resource['id']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="description">Описание</label>
            <input type="text" name="description" class="form-control" id="contact" required>
        </div>
        <input type="submit" name="submit" class="btn btn-primary" value="save">
    </form>
    </div>
    <div class="col-lg-3"></div>

<?php require_once(ROOT . '/views/footer.php'); ?>