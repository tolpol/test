<?php require_once(ROOT . '/views/header.php'); ?>

    <table class="table table-striped table-responsive">
        <thead>
        <tr class="row">
            <th class="col-lg-2">ID нтернет ресурса</th>
            <th class="col-lg-2">ID рекламного блока</th>
            <th class="col-lg-2">Дата и время</th>
            <th class="col-lg-2">Ip пользователя</th>
            <th class="col-lg-2">Код страны</th>
            <th class="col-lg-2">Количество</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($clickList as $clickItem): ?>
            <tr class="row">
                <td class="col-lg-2"><?php echo $clickItem['resource_id']; ?></td>
                <td class="col-lg-2"><?php echo $clickItem['ads_id']; ?></td>
                <td class="col-lg-2"><?php echo $clickItem['date_click']; ?></td>
                <td class="col-lg-2"><?php echo $clickItem['user_ip']; ?></td>
                <td class="col-lg-2"><?php echo $clickItem['country_code']; ?></td>
                <td class="col-lg-2"><?php echo $clickItem['quantity']; ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

<?php require_once(ROOT . '/views/footer.php'); ?>




