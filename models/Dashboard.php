<?php

class Dashboard
{
    /**
     * @return  array of summary items
     */
    public static function getCommonStatistics() {

        $db = Db::getConnection();

        $summaryList = array();

        $result = $db->prepare('
            SELECT date_show AS `date`, s.resource_id, s.ads_id,
                   s.country_code, s.quantity AS shows, click.quantity AS clicks
            FROM `show` s
                JOIN click
            WHERE s.resource_id = click.resource_id
                AND s.ads_id = click.ads_id 
                AND s.country_code = click.country_code');

        try {
            $result->execute();
        } catch (PDOException $e) {
            echo "Select failed: " . $e->getMessage();
            die();
        }

        $db = null;

        $summaryList = $result->fetchAll();

        return $summaryList;
    }
}
