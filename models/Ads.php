<?php

class Ads
{
    /**
     * @param integer $id
     * @return array $adsItem
     */
    public static function getAdsItemById($id)
    {
        $id = intval($id);

        if ($id) {
            $db = Db::getConnection();
            
            $stmt = $db->prepare('
                SELECT * 
                    FROM ads
                    WHERE id=:id');

            try {
                $stmt->execute([
                    'id' => $id,
                ]);
            } catch (PDOException $e) {
                echo "Select failed: " . $e->getMessage();
                die();
            }

            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            
            $adsItem = $stmt->fetch();

            return $adsItem;
        }
    }

    /**
     * @return  array of ads items
     */
    public static function getAdsList() {
 
        $db = Db::getConnection();
        
        $adsList = array();
        
        $result = $db->prepare('
            SELECT id, title, resource_id, description
                FROM ads
                ORDER BY id ASC');

        try {
            $result->execute();
        } catch (PDOException $e) {
            echo "Select failed: " . $e->getMessage();
            die();
        }

        $adsList = $result->fetchAll();

        return $adsList;
    }

    /**
     * @param array $ads
     * @return bool;
     */
    public static function insert($ads) {
        $db = Db::getConnection();

        $stmt = $db->prepare('
            INSERT INTO ads (title, resource_id, description)
                VALUES (:title, :resource_id, :description)');

        try {
            $stmt->execute([
                'title' => $ads['title'],
                'resource_id' => $ads['resource_id'],
                'description' => $ads['description']
            ]);
        } catch (PDOException $e) {
            echo "Inserting failed: " . $e->getMessage();
            die();
        }

        $db = null;

        return true;
    }

    /**
     * @param array $resource
     * @return bool
     */
    public static function update($resource) {
        $db = Db::getConnection();

        $stmt = $db->prepare('
            UPDATE ads
                SET title = :title, resource_id = :resource_id, description = :description
                WHERE id = :id');

        try {
            $stmt->execute([
                'id' => $resource['id'],
                'title' => $resource['title'],
                'resource_id' => $resource['resource_id'],
                'description' => $resource['description']
            ]);
        } catch (PDOException $e) {
            echo "Updating failed: " . $e->getMessage();
            die();
        }

        $pd = null;

        return true;
    }

    /**
     * @param int $id
     * @return bool
     */
    public static function delete($id) {
        $db = Db::getConnection();

        $stmt = $db->prepare('
            DELETE FROM ads
                WHERE id = :id');

        try {
            $stmt->execute([
                'id' => $id
            ]);
        } catch (PDOException $e) {
            echo "Delete failed: " . $e->getMessage();
            die();
        }

        $db = null;

        return true;
    }
}
