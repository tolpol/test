<?php

class Resource
{
    /**
     * @return array $resourceItem
     * @param integer $id
     */
    public static function getResourceItemById($id)
    {
        $id = intval($id);

        if ($id) {
            $db = Db::getConnection();
            
            $stmt = $db->prepare('
                SELECT * 
                    FROM resource
                    WHERE id=:id');
            $stmt->execute([
                'id' => $id
            ]);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            
            $resourceItem = $stmt->fetch();

            return $resourceItem;
        }
    }

    /**
     * @return array $resourceList
     */
    public static function getResourceList() {
 
        $db = Db::getConnection();
        
        $resourceList = array();
        
        $result = $db->prepare('
            SELECT id, url, topic_id AS topic, contact, 
                (SELECT group_concat(title) AS ads_title
                    FROM ads
                    WHERE resource_id = resource.id
                ) AS ads_list
            FROM resource
            ORDER BY id DESC');

        $result->execute();
        $resourceList = $result->fetchAll();

        return $resourceList;
    }

    /**
     * @return array $resourceIds
     */
    public static function getResourceIds() {

        $db = Db::getConnection();

        $resourceIds = array();

        $result = $db->prepare('
            SELECT id
                FROM resource
                ORDER BY id ASC');

        try {
            $result->execute();
        } catch (PDOException $e) {
            echo "Select failed: " . $e->getMessage();
            die();
        }

        $db = null;

        $resourceIds = $result->fetchAll();

        return $resourceIds;
    }

    /**
     * @param array $resource
     * @return bool
     */
    public static function insert($resource) {
        $db = Db::getConnection();

        $stmt = $db->prepare('
            INSERT INTO resource (url, topic_id, contact)
                VALUES (:url, :topic, :contact)');

        try {
            $stmt->execute([
                'url' => $resource['url'],
                'topic' => $resource['topic_id'],
                'contact' => $resource['contact']
            ]);
        } catch (PDOException $e) {
            echo "Inserting failed: " . $e->getMessage();
            die();
        }

        $db = null;

        return true;
    }

    /**
     * @param array $resource
     * @return bool
     */
    public static function update($resource) {
        $db = Db::getConnection();

        $stmt = $db->prepare('
            UPDATE resource
                SET url = :url, topic_id = :topic, contact = :contact
                WHERE id = :id');

        try {
            $stmt->execute([
                'id' => $resource['id'],
                'url' => $resource['url'],
                'topic' => $resource['topic_id'],
                'contact' => $resource['contact']
            ]);
        } catch (PDOException $e) {
            echo "Updating failed: " . $e->getMessage();
            die();
        }

        $db = null;

        return true;
    }

    /**
     * @param int $id
     * @return bool
     */
    public static function delete($id) {
        $db = Db::getConnection();

        $stmt = $db->prepare('
            DELETE FROM resource
                WHERE id = :id');

        try {
            $stmt->execute([
                'id' => $id
            ]);
        } catch (PDOException $e) {
            echo "Delete failed: " . $e->getMessage();
            die();
        }

        $db = null;

        return true;
    }
}
