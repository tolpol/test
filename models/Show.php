<?php

class Show
{
    /**
     * @return  array of ads items
     */
    public static function getShowList() {

        $db = Db::getConnection();

        $showList = array();

        $result = $db->prepare('
            SELECT resource_id, ads_id, date_show, user_ip, country_code, quantity
                FROM `show`');

        try {
            $result->execute();
        } catch (PDOException $e) {
            echo "Select failed: " . $e->getMessage();
            die();
        }

        $showList = $result->fetchAll();

        $db = null;

        return $showList;
    }

    /**
     * @param array $show
     * @return bool;
     */
    public static function insert($show) {
        $db = Db::getConnection();

        $stmt = $db->prepare("
            INSERT INTO `show` (resource_id, ads_id, user_ip, country_code, date_show)
                   VALUES(".$show['resource_id'].",".
                            $show['ads_id'].",'".
                            $show['user_ip']."','".
                            $show['country_code'].
                            "',NOW())
                   ON DUPLICATE KEY UPDATE quantity= quantity + 1, date_show= NOW()");

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            echo "Inserting failed: " . $e->getMessage();
            die();
        }

        $db = null;

        return true;
    }
}
