<?php

class Topic
{
    /**
     * @return array of topic items
     */
    public static function getTopicList() {
        $db = Db::getConnection();
        
        $result = $db->prepare('
            SELECT *
                FROM topic 
                ORDER BY id ASC');

        try {
            $result->execute();
        } catch (PDOException $e) {
            echo "Select failed: " . $e->getMessage();
            die();
        }

        $db = null;

        $topicList = $result->fetchAll();

        return $topicList;
    }
}
