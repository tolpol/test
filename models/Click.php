<?php

class Click
{
    /**
     * @return  array of ads items
     */
    public static function getClickList() {
 
        $db = Db::getConnection();
        
        $clickList = array();
        
        $result = $db->prepare('
            SELECT resource_id, ads_id, date_click, user_ip, country_code, quantity
                FROM click');

        try {
            $result->execute();
        } catch (PDOException $e) {
            echo "Select failed: " . $e->getMessage();
            die();
        }

        $db = null;

        $clickList = $result->fetchAll();

        return $clickList;
    }

    /**
     * @param array $click
     * @return bool;
     */
    public static function insert($click) {
        $db = Db::getConnection();

        $stmt = $db->prepare("
            INSERT INTO click (resource_id, ads_id, user_ip, country_code, date_click)
                   VALUES(".$click['resource_id'].",".
                            $click['ads_id'].",'".
                            $click['user_ip']."','".
                            $click['country_code'].
                            "',NOW())
                   ON DUPLICATE KEY UPDATE quantity= quantity + 1, date_click= NOW()");

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            echo "Insertion failed: " . $e->getMessage();
            die();
        }

        $db = null;

        return true;
    }
}
