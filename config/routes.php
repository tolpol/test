<?php

return array(

    /*
     * for default controller
     */
    '^$' => 'default/index',

    /*
     * for resource controller
     */
    'resource/index/([0-9]+)' => 'resource/index/$1',
    'resource/([0-9]+)' => 'resource/index/$1',
    'resource/new' => 'resource/new',
    'resource/edit/([0-9]+)' => 'resource/edit/$1',
    'resource/delete/([0-9]+)' => 'resource/delete/$1',

    /*
     * for ads controller
     */
    'ads/index' => 'ads/index',
    'ads/new' => 'ads/new',
    'ads/edit/([0-9]+)' => 'ads/edit/$1',
    'ads/delete/([0-9]+)' => 'ads/delete/$1',

    /*
     * for show controller
     */
    'show/index' => 'show/index',
    'show/new' => 'show/new',

    /*
     * for click controller
     */
    'click/index' => 'click/index',
    'click/new' => 'click/new',
);
