document.write('<div id="ads-container-' + window.ads_id + '"></div>');

let adsBox = document.getElementById('ads-container-' + window.ads_id);

adsBox.style.backgroundColor = window.ads_back_color;
adsBox.style.width = "200px";
adsBox.style.height = "50px";
adsBox.style.display = 'flex';
adsBox.style.alignItems = 'center';
adsBox.style.justifyContent = 'center';

let adsButton = document.createElement('button');
adsButton.innerText = 'click me!';
adsBox.appendChild(adsButton);

let adsStats = document.createElement('p');
adsBox.appendChild(adsStats);

document.addEventListener('DOMContentLoaded', function(){
    sendShowStatistic();
});

/* send show ads statistic to server */
function sendShowStatistic() {
    let xhr = new XMLHttpRequest();

    let body = 'resource_id=' + encodeURIComponent(window.resourse_id) +
        '&ads_id=' + encodeURIComponent(window.ads_id) +
        '&user_ip=' + encodeURIComponent('ip....') +
        '&country_code=' + encodeURIComponent('country_code....');

    xhr.open('POST', '../show/new', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.send(body);
}


adsButton.onclick = function() {
    sendClickStatistic();
}

/* send click ads statistic to server */
function sendClickStatistic() {
    let xhr = new XMLHttpRequest();

    let body = 'resource_id=' + encodeURIComponent(window.resourse_id) +
        '&ads_id=' + encodeURIComponent(window.ads_id) +
        '&user_ip=' + encodeURIComponent('192....') +
        '&country_code=' + encodeURIComponent('country_code....');

    xhr.open('POST', '../click/new', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.send(body);
}

