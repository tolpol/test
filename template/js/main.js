document.addEventListener('DOMContentLoaded', function(){
    highLightElem();

    if (typeof(table) === "undefined") {
        return false;
    }

    window.tBody = document.getElementById("t-body");

    table.forEach(printTable);
});

/* delete confirm */
function deleteItem () {
    let confirmed = confirm("Элемент будет удалён!");
    if (!confirmed) {
        return false;
    }
}

/* show js code */
function showAdsCode(element) {
    let urlId = element.parentElement.parentElement.children[2].innerHTML;
    let adsId = element.parentElement.parentElement.children[0].innerHTML;

    let codeModal = document.getElementById('code-modal');
    codeModal.style.display = "flex";

    document.getElementById('modal-text').innerHTML = generateAdsCode(urlId, adsId);
}

/* generate ads js code */
function generateAdsCode(urlId, adsId) {
     window.configText = '&lt;script type="text/javascript">' +
        'resourse_id = ' + urlId + ';ads_id = ' + adsId + ';ads_border = 1;' +
         'ads_back_color = "#00ff00";ads_color = "#f00";' +
        '&lt;/script&gt;';

    window.blockPathText = '&lt;script type="text/javascript" ' +
        'src="' + location.host + '/template/js/block.js"&gt;' +
        '&lt;/script&gt;';

    window.jsCode = configText + blockPathText;

    return '<pre>' + configText + blockPathText + '</pre>';
}

/* hide element */
function closeModal() {
    document.getElementById('code-modal').style.display = "none"
}

/* highlight active menu element */
function highLightElem() {
    let items = document.getElementsByClassName("menu-item");
    [].forEach.call(items, function (element) {

        let location = window.location.href;
        let link = element.firstChild.href;
        if(location == link) {
            element.className += ' active';
        }
    });
}

/* create table rows */
function printTable(item, index) {
    let tableRow = document.createElement('tr');
    tableRow.className = "row";

    let col = document.createElement('td');
    col.innerText = item.date;
    tableRow.appendChild(col);

    col = document.createElement('td');
    col.innerText = item.ads_id;
    tableRow.appendChild(col);

    col = document.createElement('td');
    col.innerHTML = item.resource_id;
    tableRow.appendChild(col);

    col = document.createElement('td');
    col.innerText = item.country_code;
    tableRow.appendChild(col);

    col = document.createElement('td');
    col.innerText = item.shows;
    tableRow.appendChild(col);

    col = document.createElement('td');
    col.innerText = item.clicks;
    tableRow.appendChild(col);

    tBody.appendChild(tableRow);
}

/* create new table according with new values */
function filter() {
    let result = table.filter(checkValue);

    tBody.innerHTML = '';

    result.forEach(printTable);
}

/* check input value according table values */
function checkValue(val) {
    /*
     * check min date
     */
    let minDate,
        minDateCheck;
    if (document.getElementById('min_date').value.length === 0) {
        minDateCheck = true
    } else {
        minDate = document.getElementById('min_date').value;
        minDateCheck = new Date(val.date) > new Date(minDate);
    }

    /*
     * check max date
     */
    let maxDate,
        maxDateCheck;
    if (document.getElementById('max_date').value.length === 0) {
        maxDateCheck = true
    } else {
        maxDate = document.getElementById('max_date').value;
        maxDateCheck = new Date(val.date) < new Date(maxDate);
    }

    /*
     * check ads_id
     */
    let adsId,
        adsCheck;
    if (document.getElementById('ads_id').value.length === 0) {
        adsCheck = true;
    } else {
        adsId = document.getElementById('ads_id').value;
        adsCheck = val.ads_id === adsId;
    }

    /*
     * check resource_id
     */
    let resourceId,
        resourceCheck;
    if (document.getElementById('resource_id').value.length === 0) {
        resourceCheck = true;
    } else {
        resourceId = document.getElementById('resource_id').value;
        resourceCheck = val.resource_id === resourceId;
    }

    /*
     * check country_code
     */
    let countryCode,
        countryCheck;
    if (document.getElementById('country_code').value.length === 0) {
        countryCheck = true;
    } else {
        countryCode = document.getElementById('country_code').value;
        countryCheck = val.country_code === countryCode;
    }

    return minDateCheck && maxDateCheck && adsCheck && resourceCheck && countryCheck;
}